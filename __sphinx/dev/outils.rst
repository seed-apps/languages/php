
##################################
les outils
##################################

* https://www.w3schools.com/

git
=======================

gestion des codes et partage

php
=======================

* https://www.w3schools.com/php/default.asp

javascript
=======================

bootstrap
---------------------------

https://getbootstrap.com/

jquery
---------------------------

* https://jquery.com/
* https://www.w3schools.com/jquery/

three.js
---------------------------

* https://threejs.org/
*https://threejs.org/docs/

css element queries
---------------------------

* https://tutorial.eyehunts.com/js/javascript-div-resize-event-example-code/
* https://marcj.github.io/css-element-queries/

graph et timeline
---------------------------

* https://visjs.org/

créer la documentation
=================================


créer la doc avec phpDocumentor
----------------------------------------


* https://www.phpdoc.org/
* https://github.com/phpDocumentor/phpDocumentor/blob/master/phpdoc.dist.xml

écrire la doc avec sphinx
---------------------------

https://lpn-doc-sphinx-primer.readthedocs.io/en/stable/concepts/heading.html


