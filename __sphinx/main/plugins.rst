
######################################
le système de plugins
######################################



fichier de description
===========================================

description du plugin dans le fichier __init__.xml


pages
=============================================

les pages principales sont des pages complètes en html
avec :

* header
* footer

l' url GET::

   ./api.php?action=page&path=[path to page]&[other args]


view
=========================================

les vues sont des fichiers xml générés par rapport à un projet
les objets xml seront traduits en js dans la page web.

l' url GET::

   ./api.php?action=view&path=[path to view]&project=[path to project]


api
=====================================

les actions sont les requetes concrètes qui executent des actions côté serveur.

l' url POST::

   ./api.php?action=api
    POST path=[path to api]




