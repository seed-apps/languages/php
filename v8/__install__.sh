# Mise à jour de Debian
sudo apt-get update
sudo apt-get upgrade -y

# Ajout du dépôt PHP 8
sudo apt-get install -y gnupg2
sudo apt-get install -y lsb-release
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sudo sh -c "echo 'deb https://packages.sury.org/php/ $(lsb_release -sc) main' > /etc/apt/sources.list.d/php.list"

# Mise à jour de PHP 7 à PHP 8
sudo apt-get update
sudo apt-get install -y php8.0
sudo apt-get install -y php8.0-fpm
sudo apt-get install -y php8.0-common
sudo apt-get install -y php8.0-cli
sudo apt-get install -y php8.0-curl
sudo apt-get install -y php8.0-mysql
sudo apt-get install -y php8.0-gd
sudo apt-get install -y php8.0-intl
sudo apt-get install -y php8.0-mbstring
sudo apt-get install -y php8.0-xml
sudo apt-get install -y php8.0-zip
sudo apt-get install -y php8.0-bcmath

# Configuration de Nginx pour utiliser PHP 8
sudo gedit /etc/nginx/sites-available/default

